#include <Arduino.h>

#include <SoftwareSerial.h>

SoftwareSerial gpsSerial(4, 5); // RX, TX for GPS

void setup() {
  // Start the serial communication with a baud rate of 9600
  Serial.begin(9600);
  // Start the GPS software serial communication with a baud rate of 9600
  gpsSerial.begin(9600);
}



String readGPSData() {
  // Read GPS data from the software serial port
  String data = "";
  while (gpsSerial.available()) {
    char c = gpsSerial.read();
    data += c;
  }
  return data;
}


void parseGPSData(String gpsData, float& latitude, float& longitude, int& altitude) {
  // Example parsing; Replace with your actual parsing logic
  // Here, assuming GPS data is in the format: $GPGGA, time, latitude, N/S indicator, longitude, E/W indicator, etc.
  int index1 = gpsData.indexOf(",");
  int index2 = gpsData.indexOf(",", index1 + 1);
  int index3 = gpsData.indexOf(",", index2 + 1);
  int index4 = gpsData.indexOf(",", index3 + 1);
  int index5 = gpsData.indexOf(",", index4 + 1);
  int index6 = gpsData.indexOf(",", index5 + 1);

  latitude = gpsData.substring(index2 + 1, index3).toFloat(); // Latitude
  if (gpsData.substring(index3 + 1, index4) == "S") {
    latitude = -latitude; // Convert to negative if south latitude
  }

  longitude = gpsData.substring(index4 + 1, index5).toFloat(); // Longitude
  if (gpsData.substring(index5 + 1, index6) == "W") {
    longitude = -longitude; // Convert to negative if west longitude
  }

  altitude = 0; // Assuming altitude is not provided in the NMEA sentence
}

String generateADSBMessage(float latitude, float longitude, int altitude, String identifier) {
  // Convert latitude, longitude, and altitude to appropriate values
  int latEnc = int((latitude + 90) / 360 * (pow(2, 32) - 1));
  int lonEnc = int((longitude + 180) / 360 * (pow(2, 32) - 1));
  int altEnc = int(altitude / 5.0); // Altitude encoding in 25-foot increments
  String debug ="ICOA ";
  debug += identifier;
  Serial.println(debug);
  // Convert identifier to hexadecimal format
  unsigned long idHex = strtoul(identifier.c_str(), nullptr, 16)& 0xFFFFFF;
// Calculate Parity Interrogator
  unsigned long pi = 0x000000 ^ (latEnc + lonEnc + altEnc + (idHex >> 16) + ((idHex >> 8) & 0xFF) + (idHex & 0xFF));

  // Construct the ADS-B message
  String adsbMessage = "";
  adsbMessage += (char)0x8D; // Downlink Format for ADS-B messages (DF = 17)
  // adsbMessage += (char)((idHex >> 16) & 0xFF); // First byte of identifier
  // adsbMessage += (char)((idHex >> 8) & 0xFF);  // Second byte of identifier
  // adsbMessage += (char)(idHex & 0xFF);         // Third byte of identifier
  adsbMessage +=identifier;  
  adsbMessage += (char)0x23; // Type Code (TC = 5)

  adsbMessage += (char)(latEnc >> 16);
  adsbMessage += (char)(latEnc >> 8);
  adsbMessage += (char)(latEnc);
  adsbMessage += (char)(lonEnc >> 16);
  adsbMessage += (char)(lonEnc >> 8);
  adsbMessage += (char)(lonEnc);
  adsbMessage += (char)(altEnc >> 8);
  adsbMessage += (char)(altEnc);
  adsbMessage += (char)(pi >> 16);
  adsbMessage += (char)(pi >> 8);
  adsbMessage += (char)(pi);

  return adsbMessage;
}


void loop() {
  // Read GPS data
  // String gpsData = readGPSData();
    String gpsData = "$GPGGA,032210.00,3251.60899,S,15145.26345,E,1,06,1.1,0.0,M,0.0,M,,*7A";

  
  // Extract latitude, longitude, altitude, and identifier from GPS data
  float latitude, longitude;
  int altitude;
  String identifier = "a12345";
  parseGPSData(gpsData, latitude, longitude, altitude);

  // Generate ADS-B message
  String adsbMessage = generateADSBMessage(latitude, longitude, altitude, identifier);

  // Print the generated ADS-B message to the normal serial port
  Serial.println("Generated ADS-B Message: " + adsbMessage);
   // Print the generated ADS-B message in hex format
  Serial.print("Generated ADS-B Message (Hex): ");
  // for (int i = 0; i < adsbMessage.length(); i++) {
  //   char hex[3];
  //   sprintf(hex, "%02X", adsbMessage[i]);
  //   Serial.print(hex);
  //   Serial.print(" ");
  // }

for (int i = 0; i < adsbMessage.length(); i++) {
    char hex[3];
    if (i != 1 && i != 2 && i != 3) {
        sprintf(hex, "%02x", (unsigned char)adsbMessage[i]);
    } else {
        sprintf(hex, "%c%c", adsbMessage[i], adsbMessage[i + 1]);
        i++; // Skip the next character as it's part of the ICAO address
    }
    Serial.print(hex);
    // Serial.print(" ");
}


  Serial.println();
  // Delay for 1 second before generating the next message
  delay(1000);
}